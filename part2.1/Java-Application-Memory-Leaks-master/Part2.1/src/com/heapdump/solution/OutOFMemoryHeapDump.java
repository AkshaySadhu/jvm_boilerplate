package com.heapdump.solution;

import java.util.ArrayList;
import java.util.List;

public class OutOFMemoryHeapDump {

	public static void main(String[] args) throws InterruptedException {
		List<ObjectForLeak> leak = new ArrayList<>();
		int count = 0;
		
		while(true) {
			leak.add(new ObjectForLeak());
			count++;
			if (count > 100) {
				System.gc();
				leak.clear();
				Thread.sleep(1000);
			} 
		}
	}

}
