package com.heapdump.solution;

import java.util.ArrayList;
import java.util.List;

public class HeapDumpMemLeakChk {

	public static void main(String[] args) throws InterruptedException {

		System.out.println("Inside main");
		List<ObjectForLeak> leak = new ArrayList<>();

		for (int i = 0; i < 1000000; i++) {
			leak.add(new ObjectForLeak());
		}
		leak.clear(); 
		Thread.sleep(10000);
		System.out.println("Post Object creation to check Memory Leak");
	}

}
