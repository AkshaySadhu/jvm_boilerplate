package com.heapdump.solution;

import java.util.ArrayList;
import java.util.Random;

public class StaticObjectReference {

	public static void main(String[] args) throws InterruptedException {
		ArrayList<Double> list = new ArrayList<Double>(1000000);
		Random random = new Random();
		for (int i = 0; i < 1000000; i++) {
			list.add(random.nextDouble());
		}
		System.gc();
		System.out.println("After GC");
		Thread.sleep(10000); // to allow GC do its job
	}

}
